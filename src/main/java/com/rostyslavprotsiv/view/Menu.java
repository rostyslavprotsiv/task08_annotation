package com.rostyslavprotsiv.view;

import java.lang.reflect.InvocationTargetException;

public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(0, "Print those fields in the class that " +
                "were annotate by my annotation.");
        menu.put(1, "Print annotation value into console.");
        menu.put(2, "Invoke method (three method with different parameters " +
                "and return types).");
        menu.put(3, "Set value into field not knowing its type.");
        menu.put(4, "Invoke myMethod(String a, int ... args) and myMethod" +
                "(String … args).");
        menu.put(5, "Show all information about unknown Class.");
        menu.put(6, "Exit");
        methodsForMenu.put(0, this::printAnnotatedFields);
        methodsForMenu.put(1, this::printAnnotationValues);
        methodsForMenu.put(2, this::printResultThreeMethods);
        methodsForMenu.put(3, this::printSetFieldValue);
        methodsForMenu.put(4, this::printResultTwoMyMethods);
        methodsForMenu.put(5, this::printAllInfo);
        methodsForMenu.put(6, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task08_Annotation");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    public void printAnnotatedFields() {
        CONTROLLER.getAnnotatedFields()
                .forEach(LOGGER::info);
    }

    public void printAnnotationValues() {
        CONTROLLER.getAnnotationValues()
                .forEach(LOGGER::info);
    }

    public void printResultThreeMethods() {
        LOGGER.info(CONTROLLER.executeThreeMethods());
    }

    public void printSetFieldValue() {
        LOGGER.info("Please, input field name : ");
        String fieldName = scan.next();
        if (fieldName.equals("name")) {
            LOGGER.info("Please, input String value : ");
            LOGGER.info(CONTROLLER.setFieldValue(fieldName, scan.next()));
        } else if (fieldName.equals("id")) {
            LOGGER.info("Please, input int value : ");
            if (scan.hasNextInt()) {
                LOGGER.info(CONTROLLER.setFieldValue(fieldName,
                        scan.nextInt()));
            } else {
                badInput();
            }
        } else if (fieldName.equals("value")) {
            LOGGER.info("Please, input double value : ");
            if (scan.hasNextDouble()) {
                LOGGER.info(CONTROLLER.setFieldValue(fieldName,
                        scan.nextDouble()));
            } else {
                badInput();
            }
        } else {
            LOGGER.error("Such field doesn't exist");
            String logMessage = "Tried to use not existing field : " + fieldName;
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
        }
    }

    public void printResultTwoMyMethods() {
        LOGGER.info(CONTROLLER.executeTwoMyMethods());
    }

    public void printAllInfo() {
        LOGGER.info("Please, input full name of the class : ");
        String fullName = scan.next();
        try {
            LOGGER.info(CONTROLLER.getAllInfo(Class.forName(fullName)
                    .getConstructor().newInstance()));
        } catch (ClassNotFoundException | IllegalAccessException
                | InstantiationException | NoSuchMethodException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
