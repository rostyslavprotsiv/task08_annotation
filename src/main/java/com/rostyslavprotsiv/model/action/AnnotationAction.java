package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.SimpleTestObject;
import com.rostyslavprotsiv.model.entity.annotations.MyFirstAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class AnnotationAction {
    private final Logger LOGGER = LogManager.getLogger(AnnotationAction.class);

    public List<String> getAnnotatedFields(Object obj) {
        List<String> fieldsInfo = new LinkedList<>();
        List<Field> annotatedFields = getFieldsWithMyAnnotations(obj);
        annotatedFields.forEach(s -> {
            try {
                s.setAccessible(true);
                fieldsInfo.add(s.getType()
                        .getSimpleName() + " " + s.getName() + " = " + s.get(obj));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        return fieldsInfo;
    }


    public List<String> getAnnotationValues(Object obj) {
        List<Field> annotatedFields = getFieldsWithMyAnnotations(obj);
        List<String> annotationsInfo = new LinkedList<>();
        annotatedFields.forEach(s -> {
                    MyFirstAnnotation ann =
                            s.getAnnotation(MyFirstAnnotation.class);
                    annotationsInfo.add("@" + ann.annotationType()
                            .getSimpleName() + "(value = \"" + ann.value() +
                            "\")");
                }
        );
        return annotationsInfo;
    }

    public String invokeThreeMethods(SimpleTestObject obj) {
        Method method[] = new Method[3];
        int resultSecondMethod = 0;
        char[] resultThirdMethod = new char[0];
        String resultOfAll;
        try {
            method[0] = SimpleTestObject.class.getDeclaredMethod("doSmth",
                    int.class, int.class);
            method[1] = SimpleTestObject.class.getDeclaredMethod("doSmth2",
                    String.class);
            method[2] = SimpleTestObject.class.getDeclaredMethod("doSmth3");
            method[0].invoke(obj, 1, 2);
            resultSecondMethod = (int) method[1].invoke(obj, "waw");
            resultThirdMethod = (char[]) method[2].invoke(obj);
        } catch (NoSuchMethodException | InvocationTargetException
                | IllegalAccessException e) {
            String logMessage = "Used incorrect methods or access denied";
            LOGGER.trace(logMessage);
            LOGGER.debug(logMessage);
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            e.printStackTrace();
        }
        resultOfAll = "First result : " + obj.getName() + ", Second result : " +
                resultSecondMethod + ", Third result : "
                + Arrays.toString(resultThirdMethod);
        return resultOfAll;
    }

    public <T> void setValueIntoField(SimpleTestObject obj, String name,
                                      T newValue) {
        try {
            Field someField = SimpleTestObject.class.getDeclaredField(name);
            someField.setAccessible(true);
            someField.set(obj, newValue);
        } catch (NoSuchFieldException | IllegalAccessException
                | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public boolean invokeTwoMyMethods(SimpleTestObject obj) {
        try {
            Method myMethodFirst = SimpleTestObject.class.getDeclaredMethod(
                    "myMethod", String.class, int[].class);
            Method myMethodSecond = SimpleTestObject.class.getDeclaredMethod(
                    "myMethod", String[].class);
            if (((int) myMethodFirst.invoke(obj, "abcs", new int[]{100, 200,
                    400, 500}))
                    >= 1000 && (boolean) myMethodSecond.invoke(obj,
                    new Object[]{new String[]{"abcd",
                            "sdf", "fgth", "btth"}})) {
                return true;
            }
        } catch (NoSuchMethodException | IllegalAccessException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
        return false;
    }

    private List<Field> getFieldsWithMyAnnotations(Object obj) {
        List<Field> annotatedFields = new LinkedList<>();
        Field[] allFields = obj.getClass()
                .getDeclaredFields();
        for (Field field : allFields) {
            if (field.isAnnotationPresent(MyFirstAnnotation.class)) {
                annotatedFields.add(field);
            }
        }
        return annotatedFields;
    }
}
