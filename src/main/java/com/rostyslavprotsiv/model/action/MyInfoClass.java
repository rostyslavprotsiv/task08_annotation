package com.rostyslavprotsiv.model.action;

import java.lang.reflect.*;

public class MyInfoClass {
    private Class clazz;

    public <T> MyInfoClass(T obj) {
        clazz = obj.getClass();
    }

    public String getInfo() {
        String allInfo = clazz.getName() + "\n" + getFields() +
                getMethods() +
                getConstructors();
        return allInfo;
    }

    private String getConstructors() {
        StringBuilder constructorsInfo = new StringBuilder();
        Constructor[] constructors = clazz.getConstructors();
        constructorsInfo.append("\n Constructors : \n");
        for (Constructor constructor : constructors) {
            constructorsInfo.append(Modifier.toString(constructor.getModifiers()))
                    .append(" ")
                    .append(clazz.getSimpleName())
                    .append("(")
                    .append(getParameters(constructor))
                    .append(")")
                    .append(" | ");
        }
        constructorsInfo.delete(constructorsInfo.length() - 3,
                constructorsInfo.length());
        return constructorsInfo.toString();
    }

    private String getFields() {
        StringBuilder fieldsInfo = new StringBuilder();
        Field[] fields = clazz.getDeclaredFields();
        fieldsInfo.append(" Fields : \n");
        for (Field field : fields) {
            fieldsInfo.append(Modifier.toString(field.getModifiers()))
                    .append(" ")
                    .append(
                            field.getType()
                                    .getSimpleName())
                    .append(" ")
                    .append(field.getName())
                    .append(" | ");
        }
        fieldsInfo.delete(fieldsInfo.length() - 3, fieldsInfo.length());
        return fieldsInfo.toString();
    }

    private String getMethods() {
        StringBuilder methodsInfo = new StringBuilder();
        Method[] methods = clazz.getDeclaredMethods();
        methodsInfo.append("\n Methods : \n");
        for (Method method : methods) {
            methodsInfo.append(Modifier.toString(method.getModifiers()))
                    .append(" ")
                    .append(
                            method.getReturnType()
                                    .getSimpleName())
                    .append(" ")
                    .append(method.getName())
                    .append("(")
                    .append(getParameters(method))
                    .append(")")
                    .append(" | ");
        }
        methodsInfo.delete(methodsInfo.length() - 3, methodsInfo.length());
        return methodsInfo.toString();
    }

    private String getParameters(Executable executable) {
        Class[] parameters = executable.getParameterTypes();
        StringBuilder allParameters = new StringBuilder();
        if (parameters.length > 0) {
            for (int i = 0; i < parameters.length; i++) {
                allParameters.append(parameters[i].getSimpleName())
                        .append(", ");
            }
            allParameters.delete(allParameters.length() - 2,
                    allParameters.length());
        }
        return allParameters.toString();
    }
}
