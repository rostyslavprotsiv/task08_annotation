package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.entity.SimpleTestObject;

public class SimpleTestObjectCreator {

    public SimpleTestObject create() {
        return new SimpleTestObject(1, "mm", 5.46);
    }
}
