package com.rostyslavprotsiv.model.entity.annotations;


import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD, ElementType.METHOD})
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface MyFirstAnnotation {
    String value() default "Value!!";
}
