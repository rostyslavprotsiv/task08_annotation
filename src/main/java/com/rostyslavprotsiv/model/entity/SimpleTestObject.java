package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.annotations.MyFirstAnnotation;

import java.util.Objects;
import java.util.stream.IntStream;

public class SimpleTestObject {
    private int id;
    @MyFirstAnnotation
    private String name;
    @MyFirstAnnotation(value = "ggg")
    private double value;

    public SimpleTestObject(int id, String name, double value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public SimpleTestObject(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public SimpleTestObject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void doSmth(int fist, int second) {
        String name = fist + " " + second;
    }

    public int doSmth2(String value) {
        return value.hashCode();
    }

    public char[] doSmth3() {
        return name.toCharArray();
    }

    public int myMethod(String a, int... args) {
        return IntStream.of(args).sum() + (int)a.charAt(1);
    }

    public boolean myMethod(String... args) {
        return args.length == args[0].length();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleTestObject that = (SimpleTestObject) o;
        return id == that.id &&
                Double.compare(that.value, value) == 0 &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, value);
    }

    @Override
    public String toString() {
        return "SimpleTestObject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
