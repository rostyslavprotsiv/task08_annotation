package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.AnnotationAction;
import com.rostyslavprotsiv.model.creator.SimpleTestObjectCreator;
import com.rostyslavprotsiv.model.action.MyInfoClass;
import com.rostyslavprotsiv.model.entity.SimpleTestObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final AnnotationAction ANNOTATION_ACTION = new AnnotationAction();
    private final SimpleTestObjectCreator SIMPLE_TEST_OBJECT_CREATOR =
            new SimpleTestObjectCreator();
    private final SimpleTestObject SIMPLE_TEST_OBJECT =
            SIMPLE_TEST_OBJECT_CREATOR.create();


    public List<String> getAnnotatedFields() {
        return ANNOTATION_ACTION.getAnnotatedFields(SIMPLE_TEST_OBJECT);
    }

    public List<String> getAnnotationValues() {
        return ANNOTATION_ACTION.getAnnotationValues(SIMPLE_TEST_OBJECT);
    }

    public String executeThreeMethods() {
        String result =
                ANNOTATION_ACTION.invokeThreeMethods(SIMPLE_TEST_OBJECT);
        if (result == null) {
            String logMessage = "Error with methods executing";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        return ANNOTATION_ACTION.invokeThreeMethods(SIMPLE_TEST_OBJECT);
    }

    public <T> String setFieldValue(String name, T value) {
        ANNOTATION_ACTION.setValueIntoField(SIMPLE_TEST_OBJECT, name, value);
        return SIMPLE_TEST_OBJECT.toString();
    }

    public boolean executeTwoMyMethods() {
        return ANNOTATION_ACTION.invokeTwoMyMethods(SIMPLE_TEST_OBJECT);
    }

    public <T> String getAllInfo(T obj) {
        final MyInfoClass myInfoClass =
                new MyInfoClass(obj);
        return myInfoClass.getInfo();
    }
}
